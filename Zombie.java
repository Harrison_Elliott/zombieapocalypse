import greenfoot.*;

/**
 * This class will ...
 * @author: Harrison Elliott
 * 
 * This is where these Zombies will start (Paired with yPos)
 */
public class Zombie extends Actor {
	/**
	 * This is where the Zombie will start on the screen (Paired with yPos)
	 */
	private short _xPos;
	/**
	 * This is where the Zombie will start on the screen (Paired with xPos)
	 */
	private short _yPos;
	/**
	 * This will be one of four values (0 to 4) 0 being North. 1 is East, 2 is South and 3 is West.
	 */
	private byte _direction;

	public void move() 
	{
		throw new UnsupportedOperationException();
	}

	public void killSurvivor() 
	{
		throw new UnsupportedOperationException();
	}

	public Zombie() 
	{
		throw new UnsupportedOperationException();
	}
}