import greenfoot.*;

/**
 * This class will ...
 * @author: Harrison Elliott
 */
public class Survivor extends Actor 
{
	private short _energyLevel;
	private byte _numberOfLives;
	private short _score;
	public Weapon _unnamed_Weapon_;

	public void move() 
	{
		throw new UnsupportedOperationException();
	}

	public void killZombie() 
	{
		throw new UnsupportedOperationException();
	}

	public void collectWeapon() 
	{
		throw new UnsupportedOperationException();
	}

	public void act() 
	{
		throw new UnsupportedOperationException();
	}

	public void useWeapon() 
	{
		throw new UnsupportedOperationException();
	}

	public void updateScore() 
	{
		throw new UnsupportedOperationException();
	}

	public void getScore() 
	{
		throw new UnsupportedOperationException();
	}

	public void getLives() 
	{
		throw new UnsupportedOperationException();
	}

	public void getEnergy() 
	{
		throw new UnsupportedOperationException();
	}

	public void killSurvivor() 
	{
		throw new UnsupportedOperationException();
	}

	public Survivor() 
	{
		throw new UnsupportedOperationException();
	}
}