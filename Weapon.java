import greenfoot.*;

/**
 * This class will ...
 * @author: Harrison Elliott
 */
public class Weapon extends Actor 
{
	public Survivor _unnamed_Survivor_;

	public void use() 
	{
		throw new UnsupportedOperationException();
	}

	public void degrade()
	{
		throw new UnsupportedOperationException();
	}

	public void collectWeapon() 
	{
		throw new UnsupportedOperationException();
	}
}