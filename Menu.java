import greenfoot.*;
import java.awt.*;

/*
 * This class will display text in a frame such
 * as the game instructions and the end game messages
 * 
 * @author: Harrison Elliott
 */
public class Menu extends Actor 
{
    // The background image of the frame
    private GreenfootImage image;

    // The height of one line of text
    private static final int LINE_HEIGHT = 20;
    
    // The left start position of a line of text
    private static final int LEFT_MARGIN = 60;
    
    // The current y position of a line of text
    private int yPos = 100;

    private Font baseFont;
    
	/**
     * Set a suitable image of an empty frame and scale
     * the image to a suitable size.  Store the current 
     * font for future use.
     */
    public Menu(int width, int height) 
	{
        setImage("frame2.png");
        image = getImage();
        image.scale(width, height);
        baseFont = image.getFont();
	}
	
	public void showEndLevel()
	{
        showHeading("Level Completed!");
        
        String [] lines =
        {
            "Well done! You have completed a level",
            "Press play to continue to the next level",
        };
        
        showLines(lines);
	    
	}
	
	public void showLoseGame(int score)
	{
        showHeading("Game Lost!");
        
        String [] lines =
        {
            "Hard luck!  You have failed to complete the game.",
            "Please click play to start playing the game again",
            "Because you were eaten, your final score is: " + score
        };
        
        showLines(lines);
    }
    
    /**
     * Display lines of text to explain how the game works,
     * what keys can be used, and how the scoring system
     * works.
     */
    public void showInstructions()
    {
        showHeading("Game Instructions");
        
        String [] lines =
        {
            "In this game you move the survivor to collect food.",
            "You complete the level when all the food is collected, ",
            "you must avoid being eaten by the Zombie.  The Zombie, ",
            "cannot see the Survivor if the Survivor is behind a Obstacle (Car)."
        };
        
        showLines(lines);
        
        showLine("");
        showKeys();
        
        showLine("");
        showScoring();
    } 
    
    /**
     * Display which keys can be used for which action when
     * the game is running
     */
    public void showKeys()
    {
        showHeading("Game Controls");
        
        String [] lines =
        {
            "Move Left: Left Arrow Key",
            "Move Right: Right Arrow Key",
            "Move Up: Up Arrow Key",
            "Move Down: Down Arrow Key",
            "Pause: Space Bar",
            "Abandon Game: Escape Key"
        };
        
        showLines(lines);
    } 
    
    /**
     * Display details of how the scoring system in the game works
     */
    public void showScoring()
    {
        showHeading("Game Scoring");
        
        String [] lines =
        {
            "20 points: Each food eaten",
            "All points lost if eaten by Zombie"
        };
        
        showLines(lines);
    } 
    
    public void showInstructions2()
    {
        showLine("In this game you move the survivor in order to collect Food.");
        showLine("You complete the level when all the food is collected, ");
        showLine("you must avoid being eaten by the Zombie. The Zombie, ");
        showLine("cannot see the Survivor if the Survivor is behind a Obstacle (Car).");
    }    
    
    public void showInstructions1()
    {
        image.drawString("In this game you move the Survivor in order to collect Food.", LEFT_MARGIN, yPos);
        yPos += LINE_HEIGHT;
        
        image.drawString("You complete the level when all the food is collected, ", LEFT_MARGIN, yPos);
        yPos += LINE_HEIGHT;
        
        image.drawString("you must avoid being eaten by the Zombie.  The Zombie, ", LEFT_MARGIN, yPos);
        yPos += LINE_HEIGHT;
        
        image.drawString("cannot see the Survivor if the Survivor is behind a Obstacle (Car).", LEFT_MARGIN, yPos);
        yPos += LINE_HEIGHT;        
    }
    
    
    /**
    *  Show one line of text drawn onto the image
    */
    private void showHeading(String line)
    {
        Font largeFont = new Font("Arial", Font.BOLD, 24);
        image.setFont(largeFont);
        image.setColor(Color.BLUE);
        showLine(line);
        
        image.setFont(baseFont);
        image.setColor(Color.BLACK);
        yPos += LINE_HEIGHT/2;
    }
    
    /**
     * Show all the lines of text stored in the lines
     * array on the background image
     */
    private void showLines(String [] lines)
    {
        for(String line : lines)
        {
            showLine(line);
        }
    }
    
    /**
    *  Show one line of text drawn onto the background image
    *  and move the y position for drawing the next line
    */
    private void showLine(String line)
    {
        image.drawString(line, LEFT_MARGIN, yPos);
        yPos += LINE_HEIGHT;
    }
    
    
    public void showWinGame(int score)
    {
        showHeading("Congratulations, You've Won!");
        
        String [] lines =
        {
            "You have won the game, having completed all the levels",
            "",
            "Your Final Score is " + score,
            "Do you want to play again?"
        };
        
        showLines(lines);
    }
    
}