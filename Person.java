import greenfoot.*;

import java.util.List;
import java.util.ArrayList;

public class Person extends Actor
{
    private static final int WALKING_SPEED = 5;

    public Person()
    {
        GreenfootImage image = getImage();
        image.scale(60, 60);
    }
    
    public void move()
    {
        move(WALKING_SPEED);
    }

    public boolean atWorldEdge()
    {
        if(getX() < 20 || getX() > getWorld().getWidth() - 20)
            return true;
        if(getY() < 20 || getY() > getWorld().getHeight() - 20)
            return true;
        else
            return false;
    }
    
    public boolean canSee(Class clss)
    {
        Actor actor = getOneObjectAtOffset(0, 0, clss);
        return actor != null;        
    }

    public void eat(Class clss)
    {
        Actor actor = getOneObjectAtOffset(0, 0, clss);
        if(actor != null) {
            getWorld().removeObject(actor);
        }
    }
}
