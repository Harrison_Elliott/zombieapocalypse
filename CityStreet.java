/**
 * This class will generate a map containing worms
 * rocks, an otter and a crab
 * 
 * @author Harrison Elliott
 * @version 1.0
 */
public class CityStreet
{
    // instance variables - replace the example below with your own
    private CityStreetWorld world;

    private Survivor Survivor;
    
    private Zombie zombie;
    
    private byte noFood;
    
    /**
     * Constructor for objects of class Citystreet
     */
    public CityStreet(CityStreetWorld world)
    {
        this.world = world;
    }

    private void addActors(String [] rows)
    {
        byte rowNo = 0;
        int width = world.getWidth() / rows[0].length();
        int height = world.getHeight() / rows.length;
        
        
        for(String row : rows)
         {
            
            for(byte pos = 0; pos < row.length(); pos++)
            {
                char c = row.charAt(pos);
                
                int x = (pos + 1) * width - width / 2;
                int y = (rowNo + 1) * height - height / 2;
                
                if(c == 'S')
                {
                    Survivor = new Survivor();
                    world.addObject(Survivor, x, y);
                }
                else if(c == '#')
                {
                    Obstacle obstacle = new Obstacle();
                    world.addObject(obstacle, x, y);
                }
                else if(c == '$')
                {
                    Food food = new Food();
                    world.addObject(food, x, y);
                    noFood++;
                }
                else if(c == 'Z')
                {
                    zombie = new Zombie();
                    world.addObject(zombie, x, y);
                }
            }
            rowNo++;
        }
    }

    
    public Survivor getSurvivor()
    {
        return Survivor;
    }
    
    public Zombie getZombie()
    {
        return zombie;
    }
    
    public int getNoFood()
    {
        return noFood;
    }
    
    /**
     * Define a map containing one Zombie (Z)
     * one Survivor (S), and a number of Obstacles (#)
     * and Food ($).  Each cell is 60 pixels
     * added to a world 960 x 720 pixels
     */
    public void setupLevel1()
    {
        String [] rows =
        {
            "................",
            "..Z.............",
            "................",
            ".............$..",
            "................",
            "................",
            "...##...........",
            "........$.......",
            "..$.........#...",
            "....$.....####..",
            "............S...",
            "....$...........",
            
        };
        
        addActors(rows);
    }

        /**
     * Define a map containing one Zombie (Z)
     * one Survivor (S), and a number of Obstacles (#)
     * and Food ($).  Each cell is 60 pixels
     * added to a world 960 x 720 pixels
     */
    public void setupLevel2()
    {
        String [] rows =
        {
            "......Z.....Z...",
            "..Z......Z......",
            "................",
            ".$...........$..",
            "................",
            "...........$....",
            ".##......##.....",
            "........$.......",
            "..$.............",
            "....S.....$$....",
            "................",
            "....$...........",
            
        };
        
        addActors(rows);
    }
    
    
    /**
     * Define a map containing one Zombie (Z)
     * one Survivor (S), and a number of Obstacles (#)
     * and Food ($).  Each cell is 60 pixels
     * added to a world 960 x 720 pixels
     */
    public void setupQuickLevel()
    {
        String [] rows =
        {
            "................",
            "..Z.........Z...",
            "..$.............",
            "............Z...",
            "......$.........",
            "................",
            "...##.......$...",
            "................",
            "............#...",
            "..........####..",
            ".........$..S...",
            "................",
            
        };
        
        addActors(rows);
    }
    
}
