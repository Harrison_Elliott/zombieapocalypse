import greenfoot.*;

/**
 * This class will ...
 * @author: Harrison Elliott
 */
public class CityStreetWorld extends World 
{
	private byte _numberOfWeapons;
	
	public CityStreetWorld() 
	{
	    super(800, 600, 1);
	}

	public void act() 
	{
		throw new UnsupportedOperationException();
	}

	public void displayInstructions() 
	{
		throw new UnsupportedOperationException();
	}

	public void displayScore() 
	{
		throw new UnsupportedOperationException();
	}

	public void displayLives() 
	{
		throw new UnsupportedOperationException();
	}

	public void winLevel() 
	{
		throw new UnsupportedOperationException();
	}

	public void winGame() 
	{
		throw new UnsupportedOperationException();
	}

	public void startLevel() 
	{
		throw new UnsupportedOperationException();
	}
}