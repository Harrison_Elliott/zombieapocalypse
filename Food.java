import greenfoot.*; 

/**
 * Write a description of class Food here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */

public class Food extends Actor 
{
	private boolean visible = true;

	/**
	 * After a period of time between 10-30 seconds the work hides if it is visible, or pops up and becomes visible.
	 */
	public void hide() 
	{
	    if(visible) visible = false;
	}

	public Food() 
	{
	    
	}
}
