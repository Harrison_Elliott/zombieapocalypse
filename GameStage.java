/**
 * Write a description of class GameStage here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */

public enum GameStage 
{
   NOT_STARTED, STARTING, PLAYING, WIN_GAME, LOSE_GAME
}
