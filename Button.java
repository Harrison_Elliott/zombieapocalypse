import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.awt.*;

/**
 * Write a description of class Button here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Button extends Actor
{
    private static final Color transparent = new Color(0,0,0,0);
    
    private GreenfootImage background;
    
    private String value;
    
    private byte fontSize = 22;
    
    private short buttonSize = 100;
    
    /**
     * Create a button with the text value centered
     * There are three sizes 0, 1, and anything else
     */
    public Button(String text)
    {
       background = getImage();  // get button image from class        
       value = text;
       updateImage();
    }
    
    /**
     * Act - do whatever the Button wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    }  
    
    /**
     * Sets the size of the font with three sizes
     * 0 = 22, 1 = 32 and anything else = 44 the
     * largest
     */
    public void setSize(byte size)
    {
        if(size == 0)
            fontSize = 22;
            
        else if(size == 1)
            fontSize = 32;
            
        else fontSize = 44;
            
        updateImage();
    }
    /**
     * Update the button image on screen to show the current text value
     * as an image drawn on top of the button image.
     */
    private void updateImage()
    {
        GreenfootImage image = new GreenfootImage(background);
        
        GreenfootImage text = new GreenfootImage(value, fontSize, Color.BLACK, transparent);
        
        image.scale(buttonSize, text.getHeight() + 20);
        
        image.drawImage(text, (image.getWidth()-text.getWidth())/2, 
                        (image.getHeight()-text.getHeight())/2);
                        
        setImage(image);
    }    
}
